import numpy as np

GOAL = 1
PENALTY = -1
STEP_COST = -0.04

DIRECTIONS = np.array(['^', '>', '<', 'v'])
UNCERTAINTY = [0.8, 0.1, 0.1]

UP = ['^', '<', '>']
RIGHT = ['>', '^', 'v']
DOWN = ['v', '>', '<']
LEFT = ['<', 'v', '^']

ACTIONS_DICT = {'^': UP, '>': RIGHT, 'v': DOWN, '<': LEFT}

np.set_printoptions(precision=3, linewidth=150)


def read_grid(filename):
    """Reads a grid file into an array.

    :param str filename: name of grid file with extension
    :returns: grid corresponding to file
    :rtype: np.array

    """
    grid = np.genfromtxt(filename, dtype='<U1')
    return np.array(grid)


def reward(grid, state):
    """Calculates the reward for an action given a state
    :param grid: the gridworld grid
    :param state: the current state
    :returns: reward
    :rtype: float

    """

    if grid[state] == 'P':
        reward = PENALTY
    elif grid[state] == 'E':
        reward = GOAL
    else:
        reward = STEP_COST
    return reward


def initial_qvalues(grid):
    """generates blank q-value matrix of 0s, except for the endstates
    """
    qvalues = np.zeros((len(DIRECTIONS), grid.shape[0], grid.shape[1]))
    for a in range(len(DIRECTIONS)):
        for y in range(grid.shape[0]):
            for x in range(grid.shape[1]):
                if grid[y, x] == 'P':
                    qvalues[a, y, x] = -1
                elif grid[y, x] == 'E':
                    qvalues[a, y, x] = 1

    return qvalues


def choose_action(epsi, state, qvalues):
    """Chooses an action based on epsilon-soft policy. Returns the action.
    """
    #    greedy_action = np.argmax([qvalues[a, state[0], state[1]] for a in range(len(DIRECTIONS))])
    action_list = np.array([qvalues[a, state[0], state[1]] for a in range(len(DIRECTIONS))])
    greedy_action = np.random.choice(np.flatnonzero(action_list == action_list.max()))
    policy = [epsi/len(DIRECTIONS) for a in range(len(DIRECTIONS))]
    policy[greedy_action] += (1 - epsi)
    action = np.random.choice(DIRECTIONS, p = policy)

    return action


def update_q_value(grid, qvalues, start_state, new_state, action, alpha, gamma):
    y, x = start_state
    new_qvalues = np.copy(qvalues)
    next_q_values = [qvalues[a, new_state[0], new_state[1]] for a in range(len(DIRECTIONS))]
    added_value = alpha * (reward(grid, start_state)
                           + gamma * max(next_q_values)
                           - qvalues[np.where(np.isin(DIRECTIONS, action)), y, x])
    new_qvalues[np.where(np.isin(DIRECTIONS, action)), y, x] += added_value
    return new_qvalues


def take_step(grid, state, tried_action):
    """Performs the action, and determines the next state.
    """
    y, x = state
    actual_action = np.random.choice(ACTIONS_DICT[tried_action], p = UNCERTAINTY)
    if actual_action == '^' and y != 0 and grid[y - 1, x] != 'O':
        new_state = (y - 1, x)
    elif actual_action == '>' and x != grid.shape[1] - 1 and grid[y, x + 1] != 'O':
        new_state = (y, x + 1)
    elif actual_action == 'v' and y != grid.shape[0] - 1 and grid[y + 1, x] != 'O':
        new_state = (y + 1, x)
    elif actual_action == '<' and x != 0 and grid[y, x - 1] != 'O':
        new_state = (y, x - 1)
    else:
        new_state = state

    return new_state


def episode(grid, state, qvalues, epsi, alpha, gamma, mode):
    view_grid = np.copy(grid)
    view_grid[state] = 'A'
    finish = False
    if mode != 'a':
        input('Press any key to begin episode')
        print(view_grid)
    while not finish:
        action = choose_action(epsi, state, qvalues)
        new_state = take_step(grid, state, action)
        view_grid[state] = action
        view_grid[new_state] = 'A'
        new_qvalues = update_q_value(grid, qvalues, state, new_state, action, alpha, gamma)
        if mode == 's':
            print('trying', action)
            print(view_grid)
            print('q-values\n', new_qvalues)
        qvalues = new_qvalues
        state = new_state
        if grid[state] != 'F':
            finish = True
        if mode == 's':
           input('Press any key to take next step')
    return qvalues


def qvalue_to_policy(grid, qvalues):
    policy = np.copy(grid)
    for y in range(policy.shape[0]):
        for x in range(policy.shape[1]):
            if grid[y, x] != 'F':
                continue
            else:
                action_index_list = np.array([qvalues[a, y, x] for a in range(len(DIRECTIONS) - 1)])
                best_action_index = np.random.choice(np.flatnonzero(action_index_list == action_index_list.max()))
#                best_action_index = np.argmax([qvalues[a, y, x] for a in range(len(DIRECTIONS) - 1)])
                policy[y, x] = DIRECTIONS[best_action_index]

    return policy


def qlearn(grid, alpha, gamma, epsi, error, stopping, mode):
    qvalues = initial_qvalues(grid)
    no_change = 0
    policy = np.copy(grid)
    while no_change < stopping:
        valid_start = False
        while not valid_start:
            state = (np.random.choice([0, 1, 2]),
                     np.random.choice([0, 1, 2, 3]))
            if grid[state] == 'F':
                valid_start = True
        new_qvalues = episode(grid, state, qvalues, epsi, alpha, gamma, mode)

        new_policy = qvalue_to_policy(grid, qvalues)
        #print(no_change)
        diff = np.abs(np.subtract(new_qvalues, qvalues))
        if np.all(diff < error):
            no_change += 1
        else:
            no_change = 0
        policy = new_policy
        qvalues = new_qvalues
        if mode == 'e':
            print('Q-values:\n', qvalues, '\npolicy:\n', policy)

    return qvalues, policy


def interactive():
    filename = str(input('Enter the grid file name with extension, e.g. 3by4.grid\n'))
    grid = read_grid(filename)
    gamma = float(input('Enter the gamma discount factor to use\n'))
    epsilon = float(input('Enter the epsilon-soft value\n'))
    alpha = float(input('Enter alpha learning rate\n'))
    mode = str(input('Choose a mode: (s)tep-wise, automatic (e)pisode, or fully (a)utomatic?\n'))
    print('Performing Q-learning on {}, with gamma = {}, alpha = {}, epsilon = {}'.format(filename, gamma, alpha, epsilon))
    policy = qlearn(grid, alpha, gamma, epsilon, 0.01, 5, mode)
    print('final q-values:\n', policy[0])
    print('final policy:\n', policy[1])


if __name__ == '__main__':
    interactive()
