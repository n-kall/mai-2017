Dependencies
============

python3 python3-numpy

How to run
==========

Ensure that the .grid files and the python script are in the same directory.

Run 'python3 gridworld.py' and follow the prompts.