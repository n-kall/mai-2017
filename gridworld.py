import numpy as np

EPSILON = 0.001

GOAL = 1
PENALTY = -1
STEP_COST = -0.04

DIRECTIONS = ['^', '>', '<', 'v']

UNCERTAINTY = [0.8, 0.1, 0.1]

UP = ['^', '<', '>']
RIGHT = ['>', '^', 'v']
DOWN = ['v', '>', '<']
LEFT = ['<', 'v', '^']


np.set_printoptions(precision=3, linewidth=150)


def read_grid(filename):
    """Reads a grid file into an array.

    :param str filename: name of grid file with extension
    :returns: grid corresponding to file
    :rtype: np.array

    """
    grid = np.genfromtxt(filename, dtype='<U1')
    return np.array(grid)


def relation(grid, state1, state2):
    """Return the relation of state1 (RELATION) state2.

    :param state1:
    :param state2:
    :returns: one of 'above', 'below', 'right_of', 'left_of']
    :rtype: str

    """
    rel = []
    if state1 == (state2[0]+1, state2[1]):
        rel.append('below')

    elif state1 == (state2[0]-1, state2[1]):
        rel.append('above')

    elif state1 == (state2[0], state2[1]+1):
        rel.append('right_of')

    elif state1 == (state2[0], state2[1]-1):
        rel.append('left_of')

    elif state1 == state2:
        y, x = state1
        if y == grid.shape[0]-1 or grid[y + 1, x] == 'O':
            rel.append('below')
        if y == 0 or grid[y - 1, x] == 'O':
            rel.append('above')
        if x == grid.shape[1]-1 or grid[y, x+1] == 'O':
            rel.append('right_of')
        if x == 0 or grid[y, x-1] == 'O':
            rel.append('left_of')

    else:
        rel.append('no_relation')

    return rel


def reward(grid, state):
    """Calculates the reward for an action given a state

    :param grid: the gridworld grid
    :param state: the current state
    :returns: reward
    :rtype: float

    """

    if grid[state] == 'P':
        reward = PENALTY
    elif grid[state] == 'E':
        reward = GOAL
    else:
        reward = STEP_COST
    return reward


def get_successors(grid, state):
    """Gives a list of possible successors from a given state.

    :param grid: the gridworld grid
    :param state: the current state
    :returns: successor states
    :rtype: list

    """

    successors = []
    y, x = state
    try:
        if y > 0 and grid[y-1, x] != 'O':
            successors.append((y-1, x))
        else:
            successors.append(state)
    except IndexError:
        pass
    try:
        if y < grid.shape[0] - 1 and grid[y+1, x] != 'O':
            successors.append((y+1, x))
        else:
            successors.append(state)
    except IndexError:
        pass
    try:
        if x > 0 and grid[y, x-1] != 'O':
            successors.append((y, x-1))
        else:
            successors.append(state)
    except IndexError:
        pass
    try:
        if y < grid.shape[1]-1 and grid[y, x+1] != 'O':
            successors.append((y, x+1))
        else:
            successors.append(state)
    except IndexError:
        pass
    return set(successors)

def trans_prob(grid, state, successor, action):
    """Calculates the probability for a state transition based on the
    action.

    :param np.array policy: the current policy
    :param tuple state: the current state
    :param tuple successor: the next state
    :returns: the probability of getting to the successor given the action
    :rtype: float

    """
    prob = 0
    # for going up
    if action == '^' and 'above' in relation(grid, successor, state):
        prob += UNCERTAINTY[0]
    if action == '^' and 'left_of' in relation(grid, successor, state):
        prob += UNCERTAINTY[1]
    if action == '^' and 'right_of' in relation(grid, successor, state):
        prob += UNCERTAINTY[2]

    # for going right
    if action == '>' and 'right_of' in relation(grid, successor, state):
        prob += UNCERTAINTY[0]
    if action == '>' and 'above' in relation(grid, successor, state):
        prob += UNCERTAINTY[1]
    if action == '>' and 'below' in relation(grid, successor, state):
        prob += UNCERTAINTY[2]

    # for going down
    if action == 'v' and 'below' in relation(grid, successor, state):
        prob += UNCERTAINTY[0]
    if action == 'v' and 'right_of' in relation(grid, successor, state):
        prob += UNCERTAINTY[1]
    if action == 'v' and 'left_of' in relation(grid, successor, state):
        prob += UNCERTAINTY[2]

    # for going left
    if action == '<' and 'left_of' in relation(grid, successor, state):
        prob += UNCERTAINTY[0]
    if action == '<' and 'below' in relation(grid, successor, state):
        prob += UNCERTAINTY[1]
    if action == '<' and 'above' in relation(grid, successor, state):
        prob += UNCERTAINTY[2]

    return prob


def blank_policy(grid):
    """Generates a new policy map from the grid.

    """
    policy = np.copy(grid)
    for y in range(len(grid)):
        for x in range(len(grid[1])):
            if grid[y, x] == 'F':
                policy[y, x] = np.random.choice(DIRECTIONS)
    return policy


def state_values(grid, policy):
    """Calculates the value of a state given a policy.

    :param grid: the grid map
    :param policy: the policy
    :param state: the state to evaluate
    :param values: the current values of states
    :param gamma: the discount factor
    :returns: a value of the state
    :rtype: float

    """
    values = np.zeros(grid.shape)
    for y in range(len(grid)):
        for x in range(len(grid[0])):
            state = (y, x)
            if grid[state] == 'O':
                continue
            elif grid[state] == 'P':
                values[state] = PENALTY
            elif grid[state] == 'E':
                values[state] = GOAL
            else:
                values[state] = reward(grid, state)

    return values


def future_values(grid, policy, values):
    """Calculates the look-ahead values of the states

    :param grid: the gridworld grid
    :param policy: the current policy
    :param values: the current values of the states
    :returns: values to be added
    :rtype: np.array

    """

    new_values = np.copy(values)
    for y in range(len(grid)):
        for x in range(len(grid[0])):
            state = (y, x)
            if grid[state] in ['O', 'P', 'E']:
                new_values[state] = 0
            else:
                future_vals = []
                action = policy[state]
                for succ in get_successors(grid, state):
                    future_val = (trans_prob(grid, state, succ, action)
                                  * values[succ])
                    future_vals.append(future_val)
                    new_values[state] = sum(future_vals)
    return new_values


def evaluate_policy_stepwise(grid, policy, gamma):
    """Evaluates a policy stepwise

    :param grid: the gridworld grid
    :param policy: the policy to be evaluated
    :param gamma: the discount factor
    :returns: values of states
    :rtype: np.array

    """

    go = True
    values = np.zeros(grid.shape)
    print(values)
    values = state_values(grid, policy)
    while go:
        go = False
        new_values = state_values(grid, policy) + \
            future_values(grid, policy, values) * gamma
        if input('next? ') == '':
            go = True
        values = np.copy(new_values)
        print(values)

    return values


def evaluate_policy(grid, policy, gamma, horizon, epsilon):
    """Evaluates a policy. Terminates when there is no change (within
    epsilon.

    :param grid: the gridworld grid
    :param policy: the policy to be evaluated
    :param gamma: the discount factor
    :param epsilon: the error value
    :returns: values
    :rtype: np.array

    """
    if horizon == 0:
        change = True
        values = np.zeros(grid.shape)
        while change:
            new_values = state_values(grid, policy) + \
                         future_values(grid, policy, values) * gamma
            diff = np.abs(np.subtract(new_values, values))
            if np.all(diff < epsilon):
                change = False
            values = np.copy(new_values)
    else:
        change = True
        values = np.zeros(grid.shape)
        iters = 0
        while iters < horizon:
            iters += 1
            new_values = state_values(grid, policy) + \
                         future_values(grid, policy, values) * gamma
            values = np.copy(new_values)

    return new_values


def best_action(grid, state, policy, values, gamma, epsilon):
    """Gets the best action based on possible changes to the policy for
    the state.

    :param grid: the gridworld grid
    :param state: current state
    :param policy: the current policy
    :param values: values array
    :param gamma: the discount factor
    :param epsilon: the error term
    :returns: action (one of '^', '>', 'v', '<')
    :rtype: str

    """
    best_val = values[state]
    best_action = policy[state]
    successors = get_successors(grid, state)
    for action in DIRECTIONS:
        new_val = 0
        for succ in successors:
            new_val += trans_prob(grid, state, succ, action) * (reward(grid, state) + values[succ])
        if new_val > best_val:
            best_val = new_val
            best_action = action

    return best_action


def improve_policy(grid, policy, values, gamma, epsilon):
    """Improves a policy by choosing the best actions for each state.

    :param grid: the gridworld grid
    :param policy: the policy to be improved
    :param values: the values of the policy to be improved
    :param gamma: the discount factor
    :param epsilon: the error value
    :returns: improved policy
    :rtype: np.array

    """
    new_pol = np.copy(policy)
    for y in range(len(new_pol)):
        for x in range(len(new_pol[0])):
            state = (y, x)
            if new_pol[state] in ['O', 'E', 'P']:
                continue
            new_pol[state] = best_action(grid, state, policy,
                                         values, gamma, epsilon)
    return new_pol


def iterate_policy(grid, gamma, epsilon, horizon, interactive='n'):
    """Performs policy evaluation and policy improvement until the policy
    is stable.

    :param grid: the gridworld grid
    :param gamma: the discount factor
    :param epsilon: the error value
    :returns:
    :rtype:

    """
    policy = blank_policy(grid)

    if interactive == 'y':
        keep_going = 'y'
        while keep_going == 'y':
            horizon = int(input('How many steps should this evaluation step perform? Enter 0 for infinite horizon.\n'))
            print('evaluating policy...')
            new_values = evaluate_policy(grid, policy, gamma, horizon,
                                         epsilon)
            print('improving policy...')
            new_policy = improve_policy(grid, policy, new_values, gamma,
                                        epsilon)
            policy = np.copy(new_policy)
            print(policy)
            print(new_values)
            keep_going = str(input('Continue? (y/n)\n'))

    elif interactive == 'n':
        change = True
        while change:
            print('evaluating policy...')
            new_values = evaluate_policy(grid, policy, gamma, horizon,
                                         epsilon)
            print('improving policy...')
            new_policy = improve_policy(grid, policy, new_values, gamma,
                                        epsilon)
            if np.all(new_policy == policy):
                change = False
            policy = np.copy(new_policy)

    print('Done!')
    return new_policy, new_values


def interactive():
    filename = str(input('Enter the grid file name with extension, e.g. 3by4.grid\n'))
    interactive = str(input('Interactive mode? (y/n)\n'))
    grid = read_grid(filename)
    gamma = float(input('Enter the gamma discount factor to use\n'))
    horizon = None
    if interactive == 'n':
        horizon = int(input('How many steps should the evaluate algorithm perform? Enter 0 for infinite horizon.\n'))
        if gamma == 1 and  horizon == 0:
            interactive = str(input('Gamma = 1 may not terminate with infinite horizon. Switch to interactive mode? (y/n)\n'))
    print('Ok, performing policy iteration on {}, with gamma = {}'.format(filename, gamma))
    policy = iterate_policy(grid, gamma, EPSILON, horizon, interactive)
    print('final policy:\n', policy[0])
    print('final values:\n', policy[1])


if __name__ == '__main__':
    interactive()
