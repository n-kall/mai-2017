import numpy as np

EPSI = 0.001

GOAL = 1
PENALTY = -1
STEP_COST = -0.04

DIRECTIONS = ['^', '>', '<', 'v']

UNCERTAINTY = [0.8, 0.1, 0.1]

UP = ['^', '<', '>']
RIGHT = ['>', '^', 'v']
DOWN = ['v', '>', '<']
LEFT = ['<', 'v', '^']


np.set_printoptions(precision=3, linewidth=150)

def read_grid(filename):
    """Reads a grid file into an array.

    :param str filename: name of grid file with extension
    :returns: grid corresponding to file
    :rtype: np.array

    """
    grid = np.genfromtxt(filename, dtype='<U1')
    return np.array(grid)
    
def relation(grid, state1, state2):
    """Return the relation of state1 (RELATION) state2.

    :param state1:
    :param state2:
    :returns: one of 'above', 'below', 'right_of', 'left_of']
    :rtype: str

    """
    rel = []
    if state1 == (state2[0]+1, state2[1]):
        rel.append('below')

    elif state1 == (state2[0]-1, state2[1]):
        rel.append('above')

    elif state1 == (state2[0], state2[1]+1):
        rel.append('right_of')

    elif state1 == (state2[0], state2[1]-1):
        rel.append('left_of')

    elif state1 == state2:
        y, x = state1
        if y == grid.shape[0]-1 or grid[y + 1, x] == 'O':
            rel.append('below')
        if y == 0 or grid[y - 1, x] == 'O':
            rel.append('above')
        if x == grid.shape[1]-1 or grid[y, x+1] == 'O':
            rel.append('right_of')
        if x == 0 or grid[y, x-1] == 'O':
            rel.append('left_of')

    else:
        rel.append('no_relation')

    return rel


def reward(grid, state):
    """Calculates the reward for an action given a state

    :param grid: the gridworld grid
    :param state: the current state
    :returns: reward
    :rtype: float

    """

    if grid[state] == 'P':
        reward = PENALTY
    elif grid[state] == 'E':
        reward = GOAL
    else:
        reward = STEP_COST
    return reward


def get_successors(grid, state):
    """Gives a list of possible successors from a given state.

    :param grid: the gridworld grid
    :param state: the current state
    :returns: successor states
    :rtype: list

    """

    successors = []
    y, x = state
    try:
        if y > 0 and grid[y-1, x] != 'O':
            successors.append((y-1, x))
        else:
            successors.append(state)
    except IndexError:
        pass
    try:
        if y < grid.shape[0] - 1 and grid[y+1, x] != 'O':
            successors.append((y+1, x))
        else:
            successors.append(state)
    except IndexError:
        pass
    try:
        if x > 0 and grid[y, x-1] != 'O':
            successors.append((y, x-1))
        else:
            successors.append(state)
    except IndexError:
        pass
    try:
        if y < grid.shape[1]-1 and grid[y, x+1] != 'O':
            successors.append((y, x+1))
        else:
            successors.append(state)
    except IndexError:
        pass
    return set(successors)


def qvalues(grid, valueList):
    """grid.shape[0] gives me the # of rows, grid.shape[1] # of rows of grid
        1. create Array with as many rows and columns as grid
        2. access each state individually
        3. update each state with valueList function
    """
        for y in range(len(qvalueArray)):
        for x in range(len(qvalueArray[0])):
            stateqvalueArray = (y, x)
            valueList(qvalueArray)
    
            
    return qvalueArray
    
def valueList(qvalueArray):  
    """ order for valueList is [top, right, bottom, left]
    
        1. Create array (List) with four slots
        2. Get qvalueArray
        3. Read specific valueList from qvalueArray
        4. TO DO ---- Update values accordidng to q-value update
        5. Update valueList
        6. Update qvalueArray
    """    
    qvalueArray = np.array((grid.shape[0],grid.shape[1])
    valueList=np.array(1,4)
    
    i=0
    for i<=3 in range(len(valueList)):
        """ TO DO update values for all directions TO DO"""
            valueList[0]=updateValueTop
            valueList[1]=updateValueRight
            valueList[2]=updateValueBottom
            valueList[3]=updateValueLeft
            qvalueArray[y,x]= valueList
            i++
    
    return valueList            
    
def select_prob(valueList, epsilon):
    """ 1. find the index i with the corresponding maximum value
        2. select action corresponding with index i
        3. get probability 1−ɛ+ɛ/4 of choosing action a*
        4. get probability ɛ/4 of choosing any other action but a*
    """
        greedyProb = 0
        i=0
        nexti=1
        for i in valueList: #i believe this does step 1. Actually I'm not sure anymore. 
            if valueList[i]<valueList[nexti]:
                i=nexti
            nexti++
                
            return i
        
        # if up is greedy
        if i == 0:
            if action == '^' and 'above' in relation(grid, successor, state):
                greedyProb += (1−epsilon + epsilon/4)
            if action == '>' and 'right_of' in relation(grid, successor, state):
                greedyProb += (epsilon/4)
            if action == 'v' and 'below' in relation(grid, successor, state):
                greedyProb += (epsilon/4)
            if action == '<' and 'left_of' in relation(grid, successor, state):
                greedyProb += (epsilon/4)
        
        # if right is greedy
        if i == 1:
            if action == '>' and 'right_of' in relation(grid, successor, state):
                greedyProb += (1-epsilon + epsilon/4)
            if action == 'v' and 'below' in relation(grid, successor, state):
                greedyProbb += (epsilon/4)
            if action == '<' and 'left_of' in relation(grid, successor, state):
                greedyProb += (epsilon/4)
            if action == '^' and 'above' in relation(grid, successor, state):
                greedyProb += (epsilon/4)
                
        # if down is greedy
        if i == 2:
            if action == 'v' and 'below' in relation(grid, successor, state):
                greedyProb += (1-epsilon + epsilon/4)
            if action == '<' and 'left_of' in relation(grid, successor, state):
                greedyProb += (epsilon/4)
            if action == '^' and 'above' in relation(grid, successor, state):
                greedyProb += (epsilon/4)
            if action == '>' and 'right_of' in relation(grid, successor, state):
                greedyProb += (epsilon/4)

        # if left is greedy
        if i == 3:
            if action == 'v' and 'below' in relation(grid, successor, state):
                greedyProb += (1-epsilon + epsilon/4)
            if action == '<' and 'left_of' in relation(grid, successor, state):
                greedyProb += (epsilon/4)
            if action == '^' and 'above' in relation(grid, successor, state):
                greedyProb += (epsilon/4)
            if action == '>' and 'right_of' in relation(grid, successor, state):
                greedyProb += (epsilon/4)

    return greedyProb

def trans_prob(grid, state, successor, action):
    """Calculates the probability for a state transition based on the
    action.

    :param np.array policy: the current policy
    :param tuple state: the current state
    :param tuple successor: the next state
    :returns: the probability of getting to the successor given the action
    :rtype: float

    """
    prob = 0
    # for going up
    if action == '^' and 'above' in relation(grid, successor, state):
        prob += UNCERTAINTY[0]
    if action == '^' and 'left_of' in relation(grid, successor, state):
        prob += UNCERTAINTY[1]
    if action == '^' and 'right_of' in relation(grid, successor, state):
        prob += UNCERTAINTY[2]

    # for going right
    if action == '>' and 'right_of' in relation(grid, successor, state):
        prob += UNCERTAINTY[0]
    if action == '>' and 'above' in relation(grid, successor, state):
        prob += UNCERTAINTY[1]
    if action == '>' and 'below' in relation(grid, successor, state):
        prob += UNCERTAINTY[2]

    # for going down
    if action == 'v' and 'below' in relation(grid, successor, state):
        prob += UNCERTAINTY[0]
    if action == 'v' and 'right_of' in relation(grid, successor, state):
        prob += UNCERTAINTY[1]
    if action == 'v' and 'left_of' in relation(grid, successor, state):
        prob += UNCERTAINTY[2]

    # for going left
    if action == '<' and 'left_of' in relation(grid, successor, state):
        prob += UNCERTAINTY[0]
    if action == '<' and 'below' in relation(grid, successor, state):
        prob += UNCERTAINTY[1]
    if action == '<' and 'above' in relation(grid, successor, state):
        prob += UNCERTAINTY[2]

    return prob


def state_values(grid, policy):
    """Calculates the value of a state given a policy.

    :param grid: the grid map
    :param policy: the policy
    :param state: the state to evaluate
    :param values: the current values of states
    :param gamma: the discount factor
    :returns: a value of the state
    :rtype: float

    """
    values = np.zeros(grid.shape)
    for y in range(len(grid)):
        for x in range(len(grid[0])):
            state = (y, x)
            if grid[state] == 'O':
                continue
            elif grid[state] == 'P':
                values[state] = PENALTY
            elif grid[state] == 'E':
                values[state] = GOAL
            else:
                values[state] = reward(grid, state)

    return values


def iterate_policy(grid, gamma, epsi, interactive='n'):
    """Performs policy evaluation and policy improvement until the policy
    is stable.

    :param grid: the gridworld grid
    :param gamma: the discount factor
    :param epsi: the error value
    :returns:
    :rtype:

    """
    policy = blank_policy(grid)

    if interactive == 'y':
        keep_going = 'y'
        while keep_going == 'y':
            print('evaluating policy...')
            horizon = int(input('How many steps should this evaluation step perform? Enter 0 for infinite horizon.\n'))
            new_values = evaluate_policy(grid, policy, gamma, horizon,
                                         epsi)
            print('improving policy...')
            new_policy = improve_policy(grid, policy, new_values, gamma,
                                        epsi)
            policy = np.copy(new_policy)
            print(policy)
            print(new_values)
            keep_going = str(input('Continue? (y/n)\n'))

    elif interactive == 'n':
        change = True
        horizon = 0
        while change:
            print('evaluating policy...')
            new_values = evaluate_policy(grid, policy, gamma, horizon,
                                         epsi)
            print('improving policy...')
            new_policy = improve_policy(grid, policy, new_values, gamma,
                                        epsi)
            if np.all(new_policy == policy):
                change = False
            policy = np.copy(new_policy)

    print('Done!')
    return new_policy, new_values


def interactive():
    filename = str(input('Enter the grid file name with extension, e.g. 3by4.grid\n'))
    grid = read_grid(filename)
    gamma = float(input('Enter the gamma discount factor to use\n'))
    interactive = str(input('Interactive mode? (y/n)\n'))
    if gamma == 1 and interactive == 'n':
        interactive = str(input('Gamma = 1 will likely not terminate. Switch to interactive mode? (y/n)\n'))
        
    epsilon = float(input('Enter the epsilon value to use\n'))
    
    print('Ok, performing policy iteration on {}, with gamma = {}'.format(filename, gamma))
    policy = iterate_policy(grid, gamma, EPSI, interactive)
    print('final policy:\n', policy[0])
    print('final values:\n', policy[1])


if __name__ == '__main__':
    interactive()
